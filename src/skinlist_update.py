#!/usr/bin/env python

import typing
from pathlib import Path


def get_skins(folder: Path, exclude: typing.Iterable = None) -> typing.List[str]:
    if exclude is None:
        exclude = ['doge', 'pokerface']
    skins = []
    for skin in folder.iterdir():
        if skin.stem not in exclude:
            skins.append(skin.stem)

    return skins


def write_skins(file_path: Path, skins: typing.List[str], sep: str = ','):
    skins_str = sep.join(skins) + '\n'
    with open(file_path, 'w') as file:
        file.write(skins_str)


def main():
    skins_folder = Path().resolve().parents[1] / 'Cigar2' / 'web' / 'skins'
    skinlist_path = Path('randomskins.txt')

    skins = get_skins(skins_folder, exclude=[])
    write_skins(skinlist_path, skins, sep='\n')


if __name__ == '__main__':
    main()
